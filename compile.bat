@ECHO off

SET SKETCH_FILE=RFnano.ino
IF [%1]==[] GOTO slave

IF %1=="-m" GOTO master

:master
ECHO [INFO]  Compiling %SKETCH_FILE% as MASTER device..
arduino-cli compile --fqbn arduino:avr:nano --libraries /src %SKETCH_FILE% --build-property build.extra_flags=-DMASTER_DEVICE
IF %ERRORLEVEL% NEQ 0 ( 
   GOTO failure
)
GOTO success

:slave
ECHO [INFO]  Compiling %SKETCH_FILE% as SLAVE device(s)..
arduino-cli compile --fqbn arduino:avr:nano --libraries /src %SKETCH_FILE%
IF %ERRORLEVEL% NEQ 0 ( 
   GOTO failure
)
GOTO success

:failure
ECHO [ERROR] Failed to compile the sketch. Please check errors above.
GOTO eof

:success
ECHO [INFO]  Sketch compiled successfully!
GOTO eof

:eof
