#!/bin/sh
SKETCH_FILE=RFnano.ino
COM=$1

if [ -z $1 ]; then
    echo no serial device
else
    echo [INFO]  Uploading sketch $SKETCH_FILE to $COM
    arduino-cli upload -p $COM --fqbn arduino:avr:nano $SKETCH_FILE
fi
