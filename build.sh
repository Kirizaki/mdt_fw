#!/bin/bash
if [ -z $1 ]; then
    echo Brak argumentów
    echo
    echo "build i upload multiple slave devices   build.sh /dev/ttyUSB1 /dev/ttyUSB2"
    echo "build and upload for master             build.sh -m /dev/ttyUSB0"
    exit
fi

if [ $1 = "-m" ]; then
    if [ -z $3 ]; then
        sh ./compile.sh -m
        sh ./upload.sh $2
    else
        echo zdefinijuj tylko jednego mastera
        exit
    fi
else
    sh ./compile.sh
    while [ "$#" -gt "0" ]
        do
            sh ./upload.sh $1
            shift
        done     
fi

