/**
 * LICENSE HERE
 */

#include "./src/DeviceController/DeviceController.h"

DeviceController device = DeviceController();

void setup() {
  device.init();
} // setup()

void loop() {
  device.loop();
} // loop
