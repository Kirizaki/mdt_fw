@ECHO off
SET COM=
SET SKETCH_FILE=RFnano.ino

SET COM=%1
IF [%COM%] == [] GOTO no_com

SET PUTTY_PROCESS=putty.exe
ECHO [INFO]  Killing %PUTTY_PROCESS% blocking %COM% port..
for /f "tokens=2" %%a in ('
    tasklist /FI "WINDOWTITLE eq %COM% - PuTTy" ^| findstr /ic:"putty.exe"
') Do taskkill /PID %%a /F

ECHO [INFO]  Uploading sketch %SKETCH_FILE% to %COM%
arduino-cli upload -p %COM% --fqbn arduino:avr:nano %SKETCH_FILE%
IF %ERRORLEVEL% NEQ 0 ( 
   GOTO failure
)
GOTO success

:no_com
ECHO [ERROR] COM port not given!
ECHO         Example: compile.bat COM1
GOTO eof

:failure
ECHO [ERROR] Failed to upload the sketch. Maybe wrong COM port?
GOTO eof

:success
ECHO [INFO]  Sketch uploaded successfully!
SET BAUD_RATE=115200
ECHO [INFO]  Running putty for %COM% at baud rate %BAUD_RATE%..
start /b putty -serial %COM% -sercfg %BAUD_RATE%
GOTO eof

:eof
