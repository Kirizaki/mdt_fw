@ECHO OFF
SET COM_COUNTER=0
SET MASTER=
SET COM_LIST=
:params_loop
@REM There is a trailing space in the next line; it is there for formatting.
IF "%1"=="-m" (
   SET MASTER=%1
) ELSE (
   SET COM_LIST=%COM_LIST%%1 
   SET /a COM_COUNTER+=1
)
SHIFT


IF NOT "%1"=="" GOTO params_loop

IF ["%COM_LIST%"]==[""] GOTO no_com

@REM Fail on multiple COM ports while compiling MASTER device
IF "%MASTER%"=="-m" IF %COM_COUNTER% NEQ 1 GOTO multiple_masters

@REM Compile
call compile.bat %MASTER%
IF %ERRORLEVEL% NEQ 0 GOTO failure ELSE GOTO upload   

:upload
@REM Upload to all boards
(for %%a in (%COM_LIST%) do (
   ECHO [INFO]  Uploading to: %%a
   call upload.bat %%a
   IF %ERRORLEVEL% NEQ 0 GOTO failure
))
GOTO success

:multiple_masters
ECHO [ERROR] Multiple COM ports given while compiling MASTER device!
ECHO         Example: build.bat COM1 -m
GOTO eof

:no_com
ECHO [ERROR] COM ports not given!
ECHO         Example: build.bat COM1 COM2 COM3 ..
GOTO eof

:failure
ECHO [ERROR] Failed to build the sketch.
GOTO eof

:success
ECHO [INFO]  Sketch built successfully!
GOTO eof

:eof
