#!/bin/sh
SKETCH_FILE=RFnano.ino

if [[ $1 = "-m" ]]
then
    echo [INFO]  Compiling $SKETCH_FILE as MASTER device..
    arduino-cli compile --fqbn arduino:avr:nano --libraries /src $SKETCH_FILE --build-property build.extra_flags=-DMASTER_DEVICE
else
    echo "[INFO]  Compiling $SKETCH_FILE as SLAVE device(s).."
    arduino-cli compile --fqbn arduino:avr:nano --libraries /src $SKETCH_FILE
fi