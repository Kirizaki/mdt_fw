#include <Arduino.h> //needed for Serial.println
#include <SPI.h>
#include "RF24.h"
#include "printf.h"

#include "./DeviceController.h"
#include "../Constants/Constants.h"
#include "../Data/Data.h"


DeviceController::DeviceController()
{
  #if defined(MASTER_DEVICE)
    m_role = MASTER_ROLE;
    m_radioNumber = MASTER_RADIO_NUMBER;
  #else
    m_role = SLAVE_ROLE;
    m_radioNumber = SLAVE_RADIO_NUMBER;
  #endif
  m_failures = 0;
}

void DeviceController::init()
{
  // if (PAYLOAD_SIZE > MAX_PAYLOAD_SIZE)
  //   #error "The maximum payload size of RF24::write has been exceeded!"

  Serial.begin(BAUD_RATE);
  while (!Serial) {
    // some boards need to wait to ensure access to serial over USB
  }

  if (m_role)
  {
    Serial.println("THIS IS MASTER DEVICE!");
  } else {
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);
    pinMode(A0, OUTPUT);
    Serial.println("THIS IS SLAVE DEVICE!");
  }
  Serial.print(F("Radio number = "));
  Serial.println((int)m_radioNumber);

  // initialize the transceiver on the SPI bus
  if (!m_radio.begin()) {
    Serial.println(F("m_radio hardware is not responding!!"));
    while (1) {} // hold in infinite loop
  }

  if (m_role) {
    Serial.println(F("TX"));
    Serial.println(F("*** PRESS R key to start transmission."));
    Serial.println(F("*** PRESS S key to stop transmission."));
    char c;
    while (!Serial.available()) {/* wait for user input */}
    while (Serial.available() && c != 'R') {
      c = toupper(Serial.read());
      if (c == 'R') {
        Serial.println(F("Transmission has been started.."));
      }
    }
  } else {
    Serial.println(F("RX"));
    Serial.println(F("Waiting for data.."));
  }

  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  m_radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // save on transmission time by setting the m_radio to only transmit the
  // number of bytes we need to transmit
  m_radio.setPayloadSize(PAYLOAD_SIZE);     // default value is the maximum 32 bytes

  // Set the TX m_address of the RX node into the TX pipe
  m_radio.openWritingPipe(m_address[m_radioNumber]);     // always uses pipe 0

  // Set the RX m_address of the TX node into a RX pipe
  m_radio.openReadingPipe(1, m_address[!m_radioNumber]); // using pipe 1

  // additional setup specific to the node's m_role
  if (m_role) {
    m_radio.stopListening();  // put m_radio in TX mode
  } else {
    m_radio.startListening(); // put m_radio in RX mode
  }
}

void DeviceController::loop()
{
  unsigned long currentTime = millis();
  if (m_role) {
    char userInput = toupper(Serial.read());
    if (userInput == 'R' && !m_running) // R -> RUN
    {
      m_running = true;
      m_counter = 0;
      m_sceneStart = currentTime;
      Serial.println(F("Start running.."));
    }

    if (userInput == 'S') // S -> STOP, reset everything
    {
      Serial.println(F("Transmission has been stopped!"));
      m_running = false;
    }

    if (!m_running)
      return;

    // This device is a TX node
    TimeData timeData = scene[m_counter];
    if (timeData.time <= currentTime - m_sceneStart)
    {
      Data data { timeData.data, timeData.command};
      m_radio.flush_tx();
      uint8_t failures = 0;
      unsigned long start_timer = micros();

      if (!m_radio.writeFast(&data, PAYLOAD_SIZE)) {
        failures++;
        m_radio.reUseTX();
      } else {
        Serial.print(F("Transmitted: "));
        Serial.print(data.data);
        Serial.print("\t");
        Serial.print(data.command);
        Serial.println("");
        m_counter++;
        if (m_counter >= COMMANDS_TO_SEND)
        {
          Serial.println(F("Transmission has been finished!"));
          m_running = false;
        }
      }

      if (failures >= 100) {
        Serial.print(F("Too many failures detected. Aborting at payload "));
        Serial.println(data.data);
        m_failures++;
        while (!Serial.available()) {/* wait for user input */}
      }

      unsigned long end_timer = micros();
      Serial.print(F("Time to transmit = "));
      Serial.print(end_timer - start_timer);
      Serial.print(F(" us with "));
      Serial.print(failures);
      Serial.println(F(" failures detected"));

      Serial.print(F("Global failure = "));
      Serial.print(m_failures);
      Serial.println(F(""));

      // to make this example readable in the serial monitor
      // delay(1000);  // slow transmissions down by 1 second
    }
  } else {
    // This device is a RX node
    if (m_radio.available()) {
      Data data;
      m_radio.read(&data, PAYLOAD_SIZE);
      if (data.command == 1)
        m_sceneStart = currentTime;

      Serial.print(F("Received: "));
      Serial.print("\t");
      Serial.print(data.data);
      Serial.print("\t");
      Serial.print(data.command);
      Serial.println("");
      Serial.print(F("currentTime - m_sceneStart: "));
      Serial.print(currentTime - m_sceneStart);
      Serial.println("");

      digitalWrite(2,data.data&1);
      digitalWrite(3,data.data&2);
      digitalWrite(4,data.data&4);
      digitalWrite(5,data.data&8);
      digitalWrite(6,data.data&16);
      digitalWrite(7,data.data&32);
      digitalWrite(8,data.data&64);
      digitalWrite(A0,data.data&128);
    }
  } // role
}
