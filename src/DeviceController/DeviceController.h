#ifndef DEVICE_CONTROLLER_H
#define DEVICE_CONTROLLER_H

#include "RF24.h"

// prototype declaration of m_radio member field
// to avoid linking to RF24.h in here (@see: DeviceController.cpp)

class DeviceController
{
  public:
    /**
     * Creates the instance of the device.
     *
     * @param m_radioNumber Defines whether the device is TX node (0) or RX node (>0).
     */
    DeviceController();
    void init();
    void loop();

    const short CN_PIN = 10;
    const short CSN_PIN = 9;
    const bool MASTER_ROLE = true;
    const bool SLAVE_ROLE = false;
    const short MASTER_RADIO_NUMBER = 0;
    const short SLAVE_RADIO_NUMBER = 1;
    const long BAUD_RATE = 115200;
    RF24 m_radio = RF24(CN_PIN, CSN_PIN);
    unsigned int m_address[2][6] = {"1Node", "2Node"};  // TODO: Name the magic numbers!
    bool m_radioNumber;
    bool m_role;
    unsigned long m_counter = 0;
    unsigned long m_sceneStart = 0;
    bool m_running = false;

    typedef unsigned int Buffer[];
    short m_failures;
    Buffer m_buffer;
};

#endif
