#ifndef DATA_H
#define DATA_H

// 2 bajty czas    1 bajt dane
// 65535           255
// troche malo ale mozna pozniej robic opoznienia miedzy stanami wysylajac czas i 0 w bajcie danych
// ale jedna akcja co minute lub co 10 jesli przymiemy rozdzielczosc 10ms jest sensowna

// 1 bajt danych
// kazde urzadzenie dekoduje swoj bit

struct TimeData
{
  unsigned long time;
  unsigned short data;
  unsigned short command;
};

struct Data
{
  unsigned short data;
  unsigned short command;
};

#define MAX_PAYLOAD_SIZE  32
const unsigned short PAYLOAD_SIZE = sizeof(Data);

/* 'time' tylko do logiki wysylania, ale do odbiornikow podajemy tylko:
  - data: dane do zapalania/zgaszania diód
*/
const TimeData scene[] = {
  /* time */  /* data */  /* command */
  {0,         0b00000000,           1},
  {100,       0b11111111,           0},
  {200,       0b00000000,           0},
  {500,       0b11111111,           0},
  {1000,      0b00000000,           0},
  {1500,      0b00000000,           0},
  {2000,      0b11111111,           0},
  {5000,      0b00000000,           0},
  {6000,      0b11111111,           0},
  {7000,      0b00000000,           0},
  {10000,     0b00000000,           0},
  {11000,     0b11111111,           0},
  {12000,     0b00000000,           0},
  {22500,     0b11111111,           0},
  {23000,     0b00000000,           0},
  {24000,     0b00000000,           0},
  {25100,     0b11111111,           0},
  {26200,     0b00000000,           0},
  {27500,     0b11111111,           0},
  {28000,     0b00000000,           0},
  {29000,     0b00000000,           0},
  {30100,     0b11111111,           0},
  {32200,     0b00000000,           0},
  {33500,     0b11111111,           0},
  {34000,     0b00000000,           0},
  {35550,     0b00000000,           0},
  {36100,     0b11111111,           0},
  {37200,     0b00000000,           0},
  {38500,     0b11111111,           0},
  {41000,     0b00000000,           0},
  {50000,     0b00000000,           0},
  {51100,     0b11111111,           0},
  {52100,     0b00000000,           0},
  {53500,     0b11111111,           0},
  {54000,     0b00000000,           0},
  {55110,     0b00000000,           0},
  {56100,     0b11111111,           0},
  {57200,     0b00000000,           0},
  {58500,     0b11111111,           0},
  {59000,     0b00000000,           0}
};

const unsigned short COMMANDS_TO_SEND = sizeof(scene) / PAYLOAD_SIZE;

#endif
